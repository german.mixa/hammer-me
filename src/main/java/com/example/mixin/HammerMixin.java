package com.example.mixin;

//import net.minecraft.client.MinecraftClient;
import net.minecraft.server.MinecraftServer;
//import net.minecraft.client.gui.screen.TitleScreen;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

//import com.example.HammerMod;
 
@Mixin(MinecraftServer.class)
public class HammerMixin {
	@Inject(at = @At("HEAD"), method = "loadWorld")
	private void init(CallbackInfo info) {
		// This code is injected into the start of MinecraftServer.loadWorld()V
		//new HammerMod();
	}
}
 