package com.example;


//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.List;




import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.fabricmc.fabric.api.itemgroup.v1.ItemGroupEvents;
import javax.tools.Tool;

//import org.apache.commons.lang3.ObjectUtils.Null;
//import org.apache.commons.lang3.ObjectUtils.Null;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//import com.google.common.base.Converter;

//import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
//import net.minecraft.entity.effect.StatusEffectInstance;
//import net.minecraft.entity.effect.StatusEffects;
//import net.minecraft.item.FoodComponent;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
//import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemGroups;
import net.minecraft.item.ItemStack;
//import net.minecraft.text.Text;
//import net.minecraft.util.Identifier;
//import net.minecraft.util.Rarity;
//import net.minecraft.registry.Registry;
//import net.minecraft.block.BlockState;
//import net.minecraft.block.entity.BlockEntity;
//import net.minecraft.util.TypedActionResult;
//import net.minecraft.item.ItemStack;
//import net.minecraft.world.World;
import net.minecraft.entity.player.PlayerEntity;
//import net.minecraft.util.Hand;
//import net.minecraft.sound.SoundEvents;
//import net.minecraft.text.Text;
//import net.minecraft.text.TextContent;
import net.minecraft.item.ToolItem;
import net.minecraft.item.PickaxeItem;
import net.minecraft.item.ToolMaterial;

import net.minecraft.util.ActionResult;
//import net.minecraft.util.BlockRotation;
import net.minecraft.item.ItemUsageContext;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
//import net.minecraft.sound.SoundCategory;
import net.minecraft.recipe.Ingredient;
import net.minecraft.item.Items;
//import net.minecraft.block.Blocks;

/* 
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraft.text.Text;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.util.Formatting;
import net.minecraft.text.TranslatableTextContent;
import net.minecraft.text.TextColor;
import net.minecraft.text.TextContent;
import net.minecraft.text.MutableText;
*/
/*
import net.minecraft.text.Text;
import net.minecraft.client.MinecraftClient;
import net.minecraft.network.message.MessageType;
import net.minecraft.client.gui.hud.ChatHud;
import net.minecraft.client.sound.ElytraSoundInstance;
import net.minecraft.text.LiteralTextContent;
import net.minecraft.util.Formatting;

import com.mojang.brigadier.LiteralMessage;
import com.mojang.brigadier.Message;

import com.mojang.brigadier.arguments.IntegerArgumentType;
import com.mojang.brigadier.arguments.StringArgumentType;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.Style;
import net.minecraft.world.GameMode;
import net.minecraft.world.World;
import net.minecraft.server.command.CommandManager;
import static com.mojang.brigadier.arguments.StringArgumentType.getString;
import static com.mojang.brigadier.arguments.StringArgumentType.word;
import static net.minecraft.server.command.CommandManager.literal;
import static net.minecraft.server.command.CommandManager.argument;
import static net.minecraft.server.command.CommandManager.*;


import net.minecraft.entity.ItemEntity;*/
import net.minecraft.block.Block;


//import net.minecraft.block.BlockState;
//import net.minecraft.world.block.*;


import net.minecraft.block.Block;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.util.Identifier;

public class HammerMod implements ModInitializer {


	public class SteelToolMaterial implements ToolMaterial
	{
		public SteelToolMaterial() {
			super();
		}

		@Override
		public int getDurability() {
			return 2500;
		}
		@Override
		public float getMiningSpeedMultiplier() {
			return 1.0F;
		}

		@Override
		public float getAttackDamage() {
			return 1.0F;
		} 
		@Override
		public int getMiningLevel() {
			return 1;
		}
		@Override
		public int getEnchantability() {
			return 30;
		}
		@Override
		public Ingredient getRepairIngredient() {
			return Ingredient.ofItems(Items.IRON_INGOT);
		}
	}
	
	public class NetheriteToolMaterial implements ToolMaterial
	{
		public NetheriteToolMaterial() {
			super();
		}

		@Override
		public int getDurability() {
			return 5000;
		}
		@Override
		public float getMiningSpeedMultiplier() {
			return 5.0F;
		}

		@Override
		public float getAttackDamage() {
			return 1.0F;
		} 
		@Override
		public int getMiningLevel() {
			return 5;
		}
		@Override
		public int getEnchantability() {
			return 30;
		}
		@Override
		public Ingredient getRepairIngredient() {
			return Ingredient.ofItems(Items.NETHERITE_BLOCK);
		}
	}
	
	
	public class NetheritePickaxe  extends PickaxeItem {
		public NetheritePickaxe(ToolMaterial material, int f, float attackSpeed, Settings _settings) {
			super(material,  f, attackSpeed, _settings);
		}

		@Override
		public ActionResult useOnBlock(ItemUsageContext context) { 


			//if (!context.getWorld().isClient()) {
				PlayerEntity player = context.getPlayer();
	
				BlockPos positionClicked = context.getBlockPos();
				Direction direction = context.getSide();
	 
					BlockPos positionClicked1 = null;
					BlockPos positionClicked2 = null;
					BlockPos positionClicked3 = null;
					BlockPos positionClicked4 = null;
					BlockPos positionClicked5 = null;
					BlockPos positionClicked6 = null;
					BlockPos positionClicked7 = null;
					BlockPos positionClicked8 = null; 

					LOGGER.info(direction.toString()); 
					//DOWN EAST NORTH SOUTH UP WEST  


				    if (direction == Direction.DOWN ||  direction == Direction.UP) 
					{
						positionClicked1 = new BlockPos(positionClicked.getX()-1, 	positionClicked.getY(), 	positionClicked.getZ()); 
						positionClicked2 = new BlockPos(positionClicked.getX()-1, 	positionClicked.getY(), 	positionClicked.getZ()-1); 
						positionClicked3 = new BlockPos(positionClicked.getX(), 	positionClicked.getY(), 	positionClicked.getZ()-1); 

						positionClicked4 = new BlockPos(positionClicked.getX()+1, 	positionClicked.getY(), 	positionClicked.getZ()); 
						positionClicked5 = new BlockPos(positionClicked.getX()+1, 	positionClicked.getY(), 	positionClicked.getZ()+1); 
						positionClicked6 = new BlockPos(positionClicked.getX(), 	positionClicked.getY(), 	positionClicked.getZ()+1); 
						
						positionClicked7 = new BlockPos(positionClicked.getX()-1, 	positionClicked.getY(), 	positionClicked.getZ()+1); 
						positionClicked8 = new BlockPos(positionClicked.getX()+1, 	positionClicked.getY(), 	positionClicked.getZ()-1); 						
					}

					else
				    if (direction == Direction.NORTH || direction == Direction.SOUTH) 
					{
						positionClicked1 = new BlockPos(positionClicked.getX()-1, 	positionClicked.getY(), 	positionClicked.getZ()); 
						positionClicked2 = new BlockPos(positionClicked.getX()-1, 	positionClicked.getY()-1, 	positionClicked.getZ()); 
						positionClicked3 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()-1, 	positionClicked.getZ()); 

						positionClicked4 = new BlockPos(positionClicked.getX()+1, 	positionClicked.getY(), 	positionClicked.getZ()); 
						positionClicked5 = new BlockPos(positionClicked.getX()+1, 	positionClicked.getY()+1, 	positionClicked.getZ()); 
						positionClicked6 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()+1, 	positionClicked.getZ()); 
						
						positionClicked7 = new BlockPos(positionClicked.getX()-1, 	positionClicked.getY()+1, 	positionClicked.getZ()); 
						positionClicked8 = new BlockPos(positionClicked.getX()+1, 	positionClicked.getY()-1, 	positionClicked.getZ()); 						
					}


					else
				    if (direction == Direction.EAST || direction == Direction.WEST) 
					{
						positionClicked1 = new BlockPos(positionClicked.getX(), 	positionClicked.getY(), 	positionClicked.getZ()-1); 
						positionClicked2 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()-1, 	positionClicked.getZ()-1); 
						positionClicked3 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()-1, 	positionClicked.getZ()); 

						positionClicked4 = new BlockPos(positionClicked.getX(), 	positionClicked.getY(), 	positionClicked.getZ()+1); 
						positionClicked5 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()+1, 	positionClicked.getZ()+1); 
						positionClicked6 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()+1, 	positionClicked.getZ()); 
						
						positionClicked7 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()+1, 	positionClicked.getZ()-1); 
						positionClicked8 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()-1, 	positionClicked.getZ()+1); 						
					}
					
					context.getWorld().breakBlock(positionClicked1, true, player);
					context.getWorld().breakBlock(positionClicked2, true, player);
					context.getWorld().breakBlock(positionClicked3, true, player);
					context.getWorld().breakBlock(positionClicked4, true, player);
					context.getWorld().breakBlock(positionClicked5, true, player);
					context.getWorld().breakBlock(positionClicked6, true, player);
					context.getWorld().breakBlock(positionClicked7, true, player);
					context.getWorld().breakBlock(positionClicked8, true, player);

					context.getWorld().breakBlock(positionClicked, true, player); 

					player.swingHand(player.getActiveHand());	
					player.swingHand(player.getActiveHand(), true);

					// define the range
					int max = 9;
					int min = 3;
					int range = max - min + 1;
					int damage = (int)(Math.random()*range + min);

					context.getStack().damage(damage, player, (playerEntity) -> player.sendToolBreakStatus(player.getActiveHand()));
					player.getItemCooldownManager().set(this, 5);
	
				//} else {
				//	player.playSound(
				//			SoundEvents.UI_TOAST_IN,
				//			SoundCategory.PLAYERS,
				//			1.0F,
				//			1.5F
				//	);
				//	player.getItemCooldownManager().set(this, 20);
				//	player.swingHand(player.getActiveHand(), true);
				//}
				
			//}
			//if (context.getWorld().isClient()) {context.getPlayer().swingHand(context.getPlayer().getActiveHand(), true);}
	
			return super.useOnBlock(context);
		}
	
	}
	
 
	public class NetheritePickaxeLarge  extends PickaxeItem {
		public NetheritePickaxeLarge(ToolMaterial material, int f, float attackSpeed, Settings _settings) {
			super(material,  f, attackSpeed, _settings);
		}

		@Override
		public ActionResult useOnBlock(ItemUsageContext context) { 


			//if (!context.getWorld().isClient()) {
				PlayerEntity player = context.getPlayer();
	
				BlockPos positionClicked = context.getBlockPos();
				Direction direction = context.getSide();
	 
					BlockPos positionClicked1 = null;
					BlockPos positionClicked2 = null;
					BlockPos positionClicked3 = null;
					BlockPos positionClicked4 = null;
					BlockPos positionClicked5 = null;
					BlockPos positionClicked6 = null;
					BlockPos positionClicked7 = null;
					BlockPos positionClicked8 = null; 

					
					BlockPos positionClicked11 = null;
					BlockPos positionClicked12 = null;
					BlockPos positionClicked13 = null;
					BlockPos positionClicked14 = null;
					BlockPos positionClicked15 = null;
					BlockPos positionClicked16 = null;
					BlockPos positionClicked17 = null;
					BlockPos positionClicked18 = null; 

					
					BlockPos positionClicked19 = null;
					BlockPos positionClicked20 = null;
					BlockPos positionClicked21 = null;
					BlockPos positionClicked22 = null;
					BlockPos positionClicked23 = null;
					BlockPos positionClicked24 = null;
					BlockPos positionClicked25 = null;
					BlockPos positionClicked26 = null; 


					LOGGER.info(direction.toString()); 
					//DOWN EAST NORTH SOUTH UP WEST  


				    if (direction == Direction.DOWN ||  direction == Direction.UP) 
					{
						positionClicked1 = new BlockPos(positionClicked.getX()-1, 	positionClicked.getY(), 	positionClicked.getZ()); 
						positionClicked2 = new BlockPos(positionClicked.getX()-1, 	positionClicked.getY(), 	positionClicked.getZ()-1); 
						positionClicked3 = new BlockPos(positionClicked.getX(), 	positionClicked.getY(), 	positionClicked.getZ()-1); 

						positionClicked4 = new BlockPos(positionClicked.getX()+1, 	positionClicked.getY(), 	positionClicked.getZ()); 
						positionClicked5 = new BlockPos(positionClicked.getX()+1, 	positionClicked.getY(), 	positionClicked.getZ()+1); 
						positionClicked6 = new BlockPos(positionClicked.getX(), 	positionClicked.getY(), 	positionClicked.getZ()+1); 
						
						positionClicked7 = new BlockPos(positionClicked.getX()-1, 	positionClicked.getY(), 	positionClicked.getZ()+1); 
						positionClicked8 = new BlockPos(positionClicked.getX()+1, 	positionClicked.getY(), 	positionClicked.getZ()-1); 	
						
						

						positionClicked11 = new BlockPos(positionClicked.getX()-2, 	positionClicked.getY(), 	positionClicked.getZ()); 
						positionClicked12 = new BlockPos(positionClicked.getX()-2, 	positionClicked.getY(), 	positionClicked.getZ()-2); 
						positionClicked13 = new BlockPos(positionClicked.getX(), 	positionClicked.getY(), 	positionClicked.getZ()-2); 

						positionClicked14 = new BlockPos(positionClicked.getX()+2, 	positionClicked.getY(), 	positionClicked.getZ()); 
						positionClicked15 = new BlockPos(positionClicked.getX()+2, 	positionClicked.getY(), 	positionClicked.getZ()+2); 
						positionClicked16 = new BlockPos(positionClicked.getX(), 	positionClicked.getY(), 	positionClicked.getZ()+2); 
						
						positionClicked17 = new BlockPos(positionClicked.getX()-2, 	positionClicked.getY(), 	positionClicked.getZ()+2); 
						positionClicked18 = new BlockPos(positionClicked.getX()+2, 	positionClicked.getY(), 	positionClicked.getZ()-2); 


						
						positionClicked19 = new BlockPos(positionClicked.getX()-1, 	positionClicked.getY(), 	positionClicked.getZ()-2); 
						positionClicked20 = new BlockPos(positionClicked.getX()-1, 	positionClicked.getY(), 	positionClicked.getZ()+2);
						positionClicked21 = new BlockPos(positionClicked.getX()+1, 	positionClicked.getY(), 	positionClicked.getZ()+2);
						positionClicked22 = new BlockPos(positionClicked.getX()+1, 	positionClicked.getY(), 	positionClicked.getZ()-2);

						positionClicked23 = new BlockPos(positionClicked.getX()-2, 	positionClicked.getY(), 	positionClicked.getZ()-1); 
						positionClicked24 = new BlockPos(positionClicked.getX()-2, 	positionClicked.getY(), 	positionClicked.getZ()+1);
						positionClicked25 = new BlockPos(positionClicked.getX()+2, 	positionClicked.getY(),		positionClicked.getZ()+1);
						positionClicked26 = new BlockPos(positionClicked.getX()+2, 	positionClicked.getY(), 	positionClicked.getZ()-1);


					}

					else
				    if (direction == Direction.NORTH || direction == Direction.SOUTH) 
					{
						positionClicked1 = new BlockPos(positionClicked.getX()-1, 	positionClicked.getY(), 	positionClicked.getZ()); 
						positionClicked2 = new BlockPos(positionClicked.getX()-1, 	positionClicked.getY()-1, 	positionClicked.getZ()); 
						positionClicked3 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()-1, 	positionClicked.getZ()); 

						positionClicked4 = new BlockPos(positionClicked.getX()+1, 	positionClicked.getY(), 	positionClicked.getZ()); 
						positionClicked5 = new BlockPos(positionClicked.getX()+1, 	positionClicked.getY()+1, 	positionClicked.getZ()); 
						positionClicked6 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()+1, 	positionClicked.getZ()); 
						
						positionClicked7 = new BlockPos(positionClicked.getX()-1, 	positionClicked.getY()+1, 	positionClicked.getZ()); 
						positionClicked8 = new BlockPos(positionClicked.getX()+1, 	positionClicked.getY()-1, 	positionClicked.getZ()); 		
						

						
						positionClicked11 = new BlockPos(positionClicked.getX()-2, 	positionClicked.getY(), 	positionClicked.getZ()); 
						positionClicked12 = new BlockPos(positionClicked.getX()-2, 	positionClicked.getY()-2, 	positionClicked.getZ()); 
						positionClicked13 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()-2, 	positionClicked.getZ()); 

						positionClicked14 = new BlockPos(positionClicked.getX()+2, 	positionClicked.getY(), 	positionClicked.getZ()); 
						positionClicked15 = new BlockPos(positionClicked.getX()+2, 	positionClicked.getY()+2, 	positionClicked.getZ()); 
						positionClicked16 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()+2, 	positionClicked.getZ()); 
						
						positionClicked17 = new BlockPos(positionClicked.getX()-2, 	positionClicked.getY()+2, 	positionClicked.getZ()); 
						positionClicked18 = new BlockPos(positionClicked.getX()+2, 	positionClicked.getY()-2, 	positionClicked.getZ()); 	

						
						positionClicked19 = new BlockPos(positionClicked.getX()-1, 	positionClicked.getY()-2, 	positionClicked.getZ()); 
						positionClicked20 = new BlockPos(positionClicked.getX()-1, 	positionClicked.getY()+2, 	positionClicked.getZ());
						positionClicked21 = new BlockPos(positionClicked.getX()+1, 	positionClicked.getY()+2, 	positionClicked.getZ());
						positionClicked22 = new BlockPos(positionClicked.getX()+1, 	positionClicked.getY()-2, 	positionClicked.getZ());

						positionClicked23 = new BlockPos(positionClicked.getX()-2, 	positionClicked.getY()-1, 	positionClicked.getZ()); 
						positionClicked24 = new BlockPos(positionClicked.getX()-2, 	positionClicked.getY()+1, 	positionClicked.getZ());
						positionClicked25 = new BlockPos(positionClicked.getX()+2, 	positionClicked.getY()+1, 	positionClicked.getZ());
						positionClicked26 = new BlockPos(positionClicked.getX()+2, 	positionClicked.getY()-1, 	positionClicked.getZ());
					}


					else
				    if (direction == Direction.EAST || direction == Direction.WEST) 
					{
						positionClicked1 = new BlockPos(positionClicked.getX(), 	positionClicked.getY(), 	positionClicked.getZ()-1); 
						positionClicked2 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()-1, 	positionClicked.getZ()-1); 
						positionClicked3 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()-1, 	positionClicked.getZ()); 

						positionClicked4 = new BlockPos(positionClicked.getX(), 	positionClicked.getY(), 	positionClicked.getZ()+1); 
						positionClicked5 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()+1, 	positionClicked.getZ()+1); 
						positionClicked6 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()+1, 	positionClicked.getZ()); 
						
						positionClicked7 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()+1, 	positionClicked.getZ()-1); 
						positionClicked8 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()-1, 	positionClicked.getZ()+1); 		
						
						
						
						positionClicked11 = new BlockPos(positionClicked.getX(), 	positionClicked.getY(), 	positionClicked.getZ()-2); 
						positionClicked12 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()-2, 	positionClicked.getZ()-2); 
						positionClicked13 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()-2, 	positionClicked.getZ()); 

						positionClicked14 = new BlockPos(positionClicked.getX(), 	positionClicked.getY(), 	positionClicked.getZ()+2); 
						positionClicked15 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()+2, 	positionClicked.getZ()+2); 
						positionClicked16 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()+2, 	positionClicked.getZ()); 
						
						positionClicked17 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()+2, 	positionClicked.getZ()-2); 
						positionClicked18 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()-2, 	positionClicked.getZ()+2); 	

						
						
						positionClicked19 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()-2, 	positionClicked.getZ()-1); 
						positionClicked20 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()+2, 	positionClicked.getZ()-1);
						positionClicked21 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()+2, 	positionClicked.getZ()+1);
						positionClicked22 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()-2, 	positionClicked.getZ()+1);

						positionClicked23 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()-1, 	positionClicked.getZ()-2); 
						positionClicked24 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()+1, 	positionClicked.getZ()-2);
						positionClicked25 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()+1, 	positionClicked.getZ()+2);
						positionClicked26 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()-1, 	positionClicked.getZ()+2);	
					}
					
					context.getWorld().breakBlock(positionClicked1, true, player);
					context.getWorld().breakBlock(positionClicked2, true, player);
					context.getWorld().breakBlock(positionClicked3, true, player);
					context.getWorld().breakBlock(positionClicked4, true, player);
					context.getWorld().breakBlock(positionClicked5, true, player);
					context.getWorld().breakBlock(positionClicked6, true, player);
					context.getWorld().breakBlock(positionClicked7, true, player);
					context.getWorld().breakBlock(positionClicked8, true, player);

					
					context.getWorld().breakBlock(positionClicked11, true, player);
					context.getWorld().breakBlock(positionClicked12, true, player);
					context.getWorld().breakBlock(positionClicked13, true, player);
					context.getWorld().breakBlock(positionClicked14, true, player);
					context.getWorld().breakBlock(positionClicked15, true, player);
					context.getWorld().breakBlock(positionClicked16, true, player);
					context.getWorld().breakBlock(positionClicked17, true, player);
					context.getWorld().breakBlock(positionClicked18, true, player);


					
					context.getWorld().breakBlock(positionClicked19, true, player);
					context.getWorld().breakBlock(positionClicked20, true, player);
					context.getWorld().breakBlock(positionClicked21, true, player);
					context.getWorld().breakBlock(positionClicked22, true, player);
					context.getWorld().breakBlock(positionClicked23, true, player);
					context.getWorld().breakBlock(positionClicked24, true, player);
					context.getWorld().breakBlock(positionClicked25, true, player);
					context.getWorld().breakBlock(positionClicked26, true, player);


					context.getWorld().breakBlock(positionClicked, true, player); 

					player.swingHand(player.getActiveHand());	
					player.swingHand(player.getActiveHand(), true);

					
					// define the range
					int max = 25;
					int min = 5;
					int range = max - min + 1;
					int damage = (int)(Math.random()*range + min);

					context.getStack().damage(damage, player, (playerEntity) -> player.sendToolBreakStatus(player.getActiveHand()));
					player.getItemCooldownManager().set(this, 5);
	
			//}
			//if (context.getWorld().isClient()) {context.getPlayer().swingHand(context.getPlayer().getActiveHand(), true);}
	
			return super.useOnBlock(context);
		}
	 
		//@Override
		//public void appendTooltip(ItemStack stack, World world, List<Text> tooltip, TooltipContext context) {
			//tooltip.add( new TranslatableText("Right-Click to mine out a 4x4x2 hole!").formatted(Formatting.GRAY));
		//	super.appendTooltip(stack, world, tooltip, context);
		//}
	}
	
	
	public class SteelPuttyKnife  extends PickaxeItem {
		public SteelPuttyKnife(ToolMaterial material, int f, float attackSpeed, Boolean IsLarge, Settings _settings) {
			super(material,  f, attackSpeed, _settings);
			_IsLarge = IsLarge;
		}

		public Boolean _IsLarge;

		@Override
		public ActionResult useOnBlock(ItemUsageContext context) { 

			PlayerEntity player = context.getPlayer();



			// убрать у пользвоателя число поставленныз блоков. Если их не зватает - ставтиь что можем
			// берём из последней первой ячейки инвентаря?
			ItemStack stack_in_8 = player.getInventory().getStack(8);
			int count_on_slot = stack_in_8.getCount();//selectedSlot(1).;


			// Item не блок при попытке конвертации в блок, превращается в "Block{minecraft:air}"
			Item item_1 = stack_in_8.getItem();
			Block block_on_8 =	Block.getBlockFromItem(item_1); 
			Item item_2 = block_on_8.asItem();

 
			//System.out.println(
			//	(Integer.parseInt(String.valueOf(Item.getRawId(item_1))) == Integer.parseInt(String.valueOf(Item.getRawId(item_2))))
			//	);





			LOGGER.info(String.valueOf(block_on_8).toLowerCase().trim());

			LOGGER.info(String.valueOf(count_on_slot));
				System.out.println(
					String.valueOf(!stack_in_8.isStackable()) + "-" +
					String.valueOf(stack_in_8.isEnchantable()) + "-" +
					String.valueOf(stack_in_8.isDamageable()) + "-" +
					String.valueOf(stack_in_8.isFood()));

 

			if (_IsLarge && count_on_slot < 16) 
			{ 
				System.out.println("Need more than 15 items on last slot");
				
				//MinecraftClient mc = MinecraftClient.getInstance();
				//ChatHud chat = new ChatHud(mc); 
				//chat.addMessage(Text.of("Need more than 7 items on last slot".toString()));	
				
			}
			else
			if (!_IsLarge && count_on_slot < 8) 
			{ 
				System.out.println("Need more than 7 items on last slot");
			}
			else

			/// хз как проверить что можно поставить, усплючаем как можем по максимум
			if (
				block_on_8 == null || 
				//String.valueOf(block_on_8).toLowerCase().trim().compareTo("Block{minecraft:air}".toLowerCase().trim()) != 0 ||
				Integer.parseInt(String.valueOf(Item.getRawId(item_1))) != Integer.parseInt(String.valueOf(Item.getRawId(item_2))) ||
				!stack_in_8.isStackable() || stack_in_8.isEnchantable() || stack_in_8.isDamageable() || stack_in_8.isFood()) 
			{ 
				System.out.println("Need block in last slot");
			}
			else
			{
				if (_IsLarge)
					stack_in_8.decrement(16);
				else
					stack_in_8.decrement(8);

	
				BlockPos positionClicked = context.getBlockPos();
				Direction direction = context.getSide();
				
	 
				BlockPos positionClicked1 = null;
				BlockPos positionClicked2 = null;
				BlockPos positionClicked3 = null;
				BlockPos positionClicked4 = null;
				BlockPos positionClicked5 = null;
				BlockPos positionClicked6 = null;
				BlockPos positionClicked7 = null;
				BlockPos positionClicked8 = null; 

				
				BlockPos positionClicked9 = null;
				BlockPos positionClicked10 = null;
				BlockPos positionClicked11 = null;
				BlockPos positionClicked12 = null;
				BlockPos positionClicked13 = null;
				BlockPos positionClicked14 = null;
				BlockPos positionClicked15 = null;
				BlockPos positionClicked16 = null; 


				//LOGGER.info(direction.toString()); 

				//DOWN EAST NORTH SOUTH UP WEST  


				if (direction == Direction.DOWN) 
				{
					int i=1;
					positionClicked1 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()+i++, 	positionClicked.getZ()); 
					positionClicked2 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()+i++, 	positionClicked.getZ()); 
					positionClicked3 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()+i++, 	positionClicked.getZ()); 
					positionClicked4 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()+i++, 	positionClicked.getZ()); 
					positionClicked5 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()+i++, 	positionClicked.getZ()); 
					positionClicked6 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()+i++, 	positionClicked.getZ()); 
					positionClicked7 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()+i++, 	positionClicked.getZ()); 
					positionClicked8 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()+i++, 	positionClicked.getZ());		
					
					
					positionClicked9 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()+i++, 	positionClicked.getZ()); 
					positionClicked10 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()+i++, 	positionClicked.getZ()); 
					positionClicked11 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()+i++, 	positionClicked.getZ()); 
					positionClicked12 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()+i++, 	positionClicked.getZ()); 
					positionClicked13 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()+i++, 	positionClicked.getZ()); 
					positionClicked14 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()+i++, 	positionClicked.getZ()); 
					positionClicked15 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()+i++, 	positionClicked.getZ()); 
					positionClicked16 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()+i++, 	positionClicked.getZ());	


				}


				else
				if (direction == Direction.UP) 
				{
					int i=1;
					positionClicked1 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()-i++, 	positionClicked.getZ()); 
					positionClicked2 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()-i++, 	positionClicked.getZ()); 
					positionClicked3 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()-i++, 	positionClicked.getZ()); 
					positionClicked4 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()-i++, 	positionClicked.getZ()); 
					positionClicked5 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()-i++, 	positionClicked.getZ()); 
					positionClicked6 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()-i++, 	positionClicked.getZ()); 
					positionClicked7 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()-i++, 	positionClicked.getZ()); 
					positionClicked8 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()-i++, 	positionClicked.getZ()); 	
					
					positionClicked9 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()-i++, 	positionClicked.getZ()); 
					positionClicked10 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()-i++, 	positionClicked.getZ()); 
					positionClicked11 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()-i++, 	positionClicked.getZ()); 
					positionClicked12 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()-i++, 	positionClicked.getZ()); 
					positionClicked13 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()-i++, 	positionClicked.getZ()); 
					positionClicked14 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()-i++, 	positionClicked.getZ()); 
					positionClicked15 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()-i++, 	positionClicked.getZ()); 
					positionClicked16 = new BlockPos(positionClicked.getX(), 	positionClicked.getY()-i++, 	positionClicked.getZ()); 			
				}


				else
				if (direction == Direction.EAST) 
				{
					int i=1;
					positionClicked1 = new BlockPos(positionClicked.getX()-i++, 	positionClicked.getY(), 	positionClicked.getZ()); 
					positionClicked2 = new BlockPos(positionClicked.getX()-i++, 	positionClicked.getY(), 	positionClicked.getZ()); 
					positionClicked3 = new BlockPos(positionClicked.getX()-i++, 	positionClicked.getY(), 	positionClicked.getZ()); 
					positionClicked4 = new BlockPos(positionClicked.getX()-i++, 	positionClicked.getY(), 	positionClicked.getZ()); 
					positionClicked5 = new BlockPos(positionClicked.getX()-i++, 	positionClicked.getY(), 	positionClicked.getZ()); 
					positionClicked6 = new BlockPos(positionClicked.getX()-i++, 	positionClicked.getY(), 	positionClicked.getZ()); 
					positionClicked7 = new BlockPos(positionClicked.getX()-i++, 	positionClicked.getY(), 	positionClicked.getZ()); 
					positionClicked8 = new BlockPos(positionClicked.getX()-i++, 	positionClicked.getY(), 	positionClicked.getZ()); 
					
					
					positionClicked9 = new BlockPos(positionClicked.getX()-i++, 	positionClicked.getY(), 	positionClicked.getZ()); 
					positionClicked10 = new BlockPos(positionClicked.getX()-i++, 	positionClicked.getY(), 	positionClicked.getZ()); 
					positionClicked11 = new BlockPos(positionClicked.getX()-i++, 	positionClicked.getY(), 	positionClicked.getZ()); 
					positionClicked12 = new BlockPos(positionClicked.getX()-i++, 	positionClicked.getY(), 	positionClicked.getZ()); 
					positionClicked13 = new BlockPos(positionClicked.getX()-i++, 	positionClicked.getY(), 	positionClicked.getZ()); 
					positionClicked14 = new BlockPos(positionClicked.getX()-i++, 	positionClicked.getY(), 	positionClicked.getZ());
					positionClicked15 = new BlockPos(positionClicked.getX()-i++, 	positionClicked.getY(), 	positionClicked.getZ()); 
					positionClicked16 = new BlockPos(positionClicked.getX()-i++, 	positionClicked.getY(), 	positionClicked.getZ()); 
				}


				else
				if (direction == Direction.WEST) /// +++
				{
					int i=1;
					positionClicked1 = new BlockPos(positionClicked.getX()+i++, 	positionClicked.getY(), 	positionClicked.getZ()); 
					positionClicked2 = new BlockPos(positionClicked.getX()+i++, 	positionClicked.getY(), 	positionClicked.getZ()); 
					positionClicked3 = new BlockPos(positionClicked.getX()+i++, 	positionClicked.getY(), 	positionClicked.getZ()); 
					positionClicked4 = new BlockPos(positionClicked.getX()+i++, 	positionClicked.getY(), 	positionClicked.getZ()); 
					positionClicked5 = new BlockPos(positionClicked.getX()+i++, 	positionClicked.getY(), 	positionClicked.getZ()); 
					positionClicked6 = new BlockPos(positionClicked.getX()+i++, 	positionClicked.getY(), 	positionClicked.getZ());
					positionClicked7 = new BlockPos(positionClicked.getX()+i++, 	positionClicked.getY(), 	positionClicked.getZ()); 
					positionClicked8 = new BlockPos(positionClicked.getX()+i++, 	positionClicked.getY(), 	positionClicked.getZ()); 		
					
					
					positionClicked9 = new BlockPos(positionClicked.getX()+i++, 	positionClicked.getY(), 	positionClicked.getZ()); 
					positionClicked10 = new BlockPos(positionClicked.getX()+i++, 	positionClicked.getY(), 	positionClicked.getZ()); 
					positionClicked11 = new BlockPos(positionClicked.getX()+i++, 	positionClicked.getY(), 	positionClicked.getZ()); 
					positionClicked12 = new BlockPos(positionClicked.getX()+i++, 	positionClicked.getY(), 	positionClicked.getZ()); 
					positionClicked13 = new BlockPos(positionClicked.getX()+i++, 	positionClicked.getY(), 	positionClicked.getZ()); 
					positionClicked14 = new BlockPos(positionClicked.getX()+i++, 	positionClicked.getY(), 	positionClicked.getZ()); 
					positionClicked15 = new BlockPos(positionClicked.getX()+i++, 	positionClicked.getY(), 	positionClicked.getZ());
					positionClicked16 = new BlockPos(positionClicked.getX()+i++, 	positionClicked.getY(), 	positionClicked.getZ()); 
				}


				else
				if (direction == Direction.NORTH) 
				{
					int i=1;
					positionClicked1 = new BlockPos(positionClicked.getX(), 	positionClicked.getY(), 	positionClicked.getZ()+i++); 
					positionClicked2 = new BlockPos(positionClicked.getX(), 	positionClicked.getY(), 	positionClicked.getZ()+i++); 
					positionClicked3 = new BlockPos(positionClicked.getX(), 	positionClicked.getY(), 	positionClicked.getZ()+i++); 
					positionClicked4 = new BlockPos(positionClicked.getX(), 	positionClicked.getY(), 	positionClicked.getZ()+i++); 
					positionClicked5 = new BlockPos(positionClicked.getX(), 	positionClicked.getY(), 	positionClicked.getZ()+i++); 
					positionClicked6 = new BlockPos(positionClicked.getX(), 	positionClicked.getY(), 	positionClicked.getZ()+i++); 
					positionClicked7 = new BlockPos(positionClicked.getX(), 	positionClicked.getY(), 	positionClicked.getZ()+i++); 
					positionClicked8 = new BlockPos(positionClicked.getX(), 	positionClicked.getY(), 	positionClicked.getZ()+i++);

				
					positionClicked9 = new BlockPos(positionClicked.getX(), 	positionClicked.getY(), 	positionClicked.getZ()+i++); 
					positionClicked10 = new BlockPos(positionClicked.getX(), 	positionClicked.getY(), 	positionClicked.getZ()+i++); 
					positionClicked11 = new BlockPos(positionClicked.getX(), 	positionClicked.getY(), 	positionClicked.getZ()+i++); 
					positionClicked12 = new BlockPos(positionClicked.getX(), 	positionClicked.getY(), 	positionClicked.getZ()+i++); 
					positionClicked13 = new BlockPos(positionClicked.getX(), 	positionClicked.getY(), 	positionClicked.getZ()+i++); 
					positionClicked14 = new BlockPos(positionClicked.getX(), 	positionClicked.getY(), 	positionClicked.getZ()+i++); 
					positionClicked15 = new BlockPos(positionClicked.getX(), 	positionClicked.getY(), 	positionClicked.getZ()+i++); 
					positionClicked16 = new BlockPos(positionClicked.getX(), 	positionClicked.getY(), 	positionClicked.getZ()+i++); 	
				}


				else
				if (direction == Direction.SOUTH) 
				{
					int i=1;
					positionClicked1 = new BlockPos(positionClicked.getX(), 	positionClicked.getY(), 	positionClicked.getZ()-i++); 
					positionClicked2 = new BlockPos(positionClicked.getX(), 	positionClicked.getY(), 	positionClicked.getZ()-i++); 
					positionClicked3 = new BlockPos(positionClicked.getX(), 	positionClicked.getY(), 	positionClicked.getZ()-i++); 
					positionClicked4 = new BlockPos(positionClicked.getX(), 	positionClicked.getY(), 	positionClicked.getZ()-i++); 
					positionClicked5 = new BlockPos(positionClicked.getX(), 	positionClicked.getY(), 	positionClicked.getZ()-i++); 
					positionClicked6 = new BlockPos(positionClicked.getX(), 	positionClicked.getY(), 	positionClicked.getZ()-i++);
					positionClicked7 = new BlockPos(positionClicked.getX(), 	positionClicked.getY(), 	positionClicked.getZ()-i++); 
					positionClicked8 = new BlockPos(positionClicked.getX(), 	positionClicked.getY(), 	positionClicked.getZ()-i++); 	
					
					
					positionClicked9 = new BlockPos(positionClicked.getX(), 	positionClicked.getY(), 	positionClicked.getZ()-i++); 
					positionClicked10 = new BlockPos(positionClicked.getX(), 	positionClicked.getY(), 	positionClicked.getZ()-i++); 
					positionClicked11 = new BlockPos(positionClicked.getX(), 	positionClicked.getY(), 	positionClicked.getZ()-i++); 
					positionClicked12 = new BlockPos(positionClicked.getX(), 	positionClicked.getY(), 	positionClicked.getZ()-i++); 
					positionClicked13 = new BlockPos(positionClicked.getX(), 	positionClicked.getY(), 	positionClicked.getZ()-i++); 
					positionClicked14 = new BlockPos(positionClicked.getX(), 	positionClicked.getY(), 	positionClicked.getZ()-i++);
					positionClicked15 = new BlockPos(positionClicked.getX(), 	positionClicked.getY(), 	positionClicked.getZ()-i++); 
					positionClicked16 = new BlockPos(positionClicked.getX(), 	positionClicked.getY(), 	positionClicked.getZ()-i++); 
				}
				
				/// если spawnEntity - то выпадает как предмет
				
				//context.getWorld().spawnEntity(new ItemEntity(context.getWorld(), positionClicked1.getX(), positionClicked1.getY(), positionClicked1.getZ(), stack_in_8));
				

			 
				//context.getWorld().setBlockState(positionClicked, Blocks.STONE.getDefaultState()); 


				

				
				context.getWorld().breakBlock(positionClicked1, true, player);
				context.getWorld().breakBlock(positionClicked2, true, player);
				context.getWorld().breakBlock(positionClicked3, true, player);
				context.getWorld().breakBlock(positionClicked4, true, player);
				context.getWorld().breakBlock(positionClicked5, true, player);
				context.getWorld().breakBlock(positionClicked6, true, player);
				context.getWorld().breakBlock(positionClicked7, true, player);
				context.getWorld().breakBlock(positionClicked8, true, player);




				//positionClicked = positionClicked.rotate(BlockRotation.CLOCKWISE_90);
				//positionClicked1 = positionClicked1.rotate(BlockRotation.COUNTERCLOCKWISE_90);
				//positionClicked2 = positionClicked2.rotate(BlockRotation.NONE);
				//positionClicked3 = positionClicked3.rotate(BlockRotation.CLOCKWISE_180);




				context.getWorld().setBlockState(positionClicked1, block_on_8.getDefaultState());
				context.getWorld().setBlockState(positionClicked2, block_on_8.getDefaultState());
				context.getWorld().setBlockState(positionClicked3, block_on_8.getDefaultState());
				context.getWorld().setBlockState(positionClicked4, block_on_8.getDefaultState());
				context.getWorld().setBlockState(positionClicked5, block_on_8.getDefaultState());
				context.getWorld().setBlockState(positionClicked6, block_on_8.getDefaultState());
				context.getWorld().setBlockState(positionClicked7, block_on_8.getDefaultState());
				context.getWorld().setBlockState(positionClicked8, block_on_8.getDefaultState());					
				
				

				if (_IsLarge)
				{					
					context.getWorld().breakBlock(positionClicked9, true, player);
					context.getWorld().breakBlock(positionClicked10, true, player);
					context.getWorld().breakBlock(positionClicked11, true, player);
					context.getWorld().breakBlock(positionClicked12, true, player);
					context.getWorld().breakBlock(positionClicked13, true, player);
					context.getWorld().breakBlock(positionClicked14, true, player);
					context.getWorld().breakBlock(positionClicked15, true, player);
					context.getWorld().breakBlock(positionClicked16, true, player);


					context.getWorld().setBlockState(positionClicked9, block_on_8.getDefaultState());
					context.getWorld().setBlockState(positionClicked10, block_on_8.getDefaultState());
					context.getWorld().setBlockState(positionClicked11, block_on_8.getDefaultState());
					context.getWorld().setBlockState(positionClicked12, block_on_8.getDefaultState());
					context.getWorld().setBlockState(positionClicked13, block_on_8.getDefaultState());
					context.getWorld().setBlockState(positionClicked14, block_on_8.getDefaultState());
					context.getWorld().setBlockState(positionClicked15, block_on_8.getDefaultState());
					context.getWorld().setBlockState(positionClicked16, block_on_8.getDefaultState());					
				}


				player.swingHand(player.getActiveHand());	
				player.swingHand(player.getActiveHand(), true);

				context.getStack().damage(1, player, (playerEntity) -> player.sendToolBreakStatus(player.getActiveHand()));
				player.getItemCooldownManager().set(this, 5);

			}
			return super.useOnBlock(context);
		}

	}

	

	public static final Logger LOGGER = LoggerFactory.getLogger("hammerme");
	public NetheriteToolMaterial 	NetheriteMaterial = new NetheriteToolMaterial();
	public SteelToolMaterial 		SteelMaterial = new SteelToolMaterial();



	public ToolItem 		 	HAMMER_NEZER = new NetheritePickaxe
	(
		NetheriteMaterial, 
		(int) NetheriteMaterial.getAttackDamage(),
		3.0F,
		new Item.Settings()

		//new Item(new Item.Settings().group(ItemGroup.MISC));
		//ItemGroupEvents.modifyEntriesEvent(ItemGroups.INGREDIENTS).register(entries -> entries.add(MY_ITEM))
	);
		
	public ToolItem 		 	HAMMER_NEZER_LARGE = new NetheritePickaxeLarge
	(
		NetheriteMaterial, 
		(int) NetheriteMaterial.getAttackDamage(),
		3.0F,
		new Item.Settings()
	);


	
	public ToolItem 		 	PUTTY_KNIFE = new SteelPuttyKnife
	(
		SteelMaterial, 
		(int) SteelMaterial.getAttackDamage(),
		1.0F,
		false,
		new Item.Settings()
	);

	
	
	public ToolItem 		 	PUTTY_KNIFE_LARGE = new SteelPuttyKnife
	(
		SteelMaterial, 
		(int) SteelMaterial.getAttackDamage(),
		1.0F,
		true,
		new Item.Settings()
	);




	@Override
	public void onInitialize() {
		// This code runs as soon as Minecraft is in a mod-load-ready state.
		// However, some things (like resources) may still be uninitialized.
		// Proceed with mild caution.
		System.out.println("Hello HammerMod!");

		
		LOGGER.info("HammerMod start..."); 
		
        Registry.register(Registries.ITEM, new Identifier("hammerme", "hammer_nether"), HAMMER_NEZER);
        Registry.register(Registries.ITEM, new Identifier("hammerme", "hammer_nether_large"), HAMMER_NEZER_LARGE);
        Registry.register(Registries.ITEM, new Identifier("hammerme", "putty_knife"), PUTTY_KNIFE);
        Registry.register(Registries.ITEM, new Identifier("hammerme", "putty_knife_large"), PUTTY_KNIFE_LARGE);

		ItemGroupEvents.modifyEntriesEvent(ItemGroups.TOOLS).register(entries -> entries.add(HAMMER_NEZER));
		ItemGroupEvents.modifyEntriesEvent(ItemGroups.TOOLS).register(entries -> entries.add(HAMMER_NEZER_LARGE));
		ItemGroupEvents.modifyEntriesEvent(ItemGroups.TOOLS).register(entries -> entries.add(PUTTY_KNIFE));
		ItemGroupEvents.modifyEntriesEvent(ItemGroups.TOOLS).register(entries -> entries.add(PUTTY_KNIFE_LARGE));

		//ItemGroupEvents.modifyEntriesEvent(ItemGroups.INGREDIENTS).register(entries -> entries.add(HAMMER_NEZER));
		//ItemGroupEvents.modifyEntriesEvent(ItemGroups.INGREDIENTS).register(entries -> entries.add(HAMMER_NEZER_LARGE));
		//ItemGroupEvents.modifyEntriesEvent(ItemGroups.INGREDIENTS).register(entries -> entries.add(PUTTY_KNIFE));
		//ItemGroupEvents.modifyEntriesEvent(ItemGroups.INGREDIENTS).register(entries -> entries.add(PUTTY_KNIFE_LARGE));

		LOGGER.info("HammerMod loaded");
		
	}

}
