# Hammer Me

Inspire mod named by Vanilla Hammers
Add to game 2 hammer: large and simple.
- On left button they mining 1 block, but on right button its 3x3 and 5x5
- Durability is 5000, and spend random(3,9) or random(5,25) points for hammer or large hammer.
- MiningLevel=5
- Enchantability=30

Putty knife (Putty knife large) put 8 (or 16) blocks from you last position inventary (it must be block not less 8 or 16).

ONLY Fabric

PS Inspite Vanilla Hammers mod =)

# Screenshots

<img src="/src/readme/hammer.png">
<img src="/src/readme/hammer_large.png">
<img src="/src/readme/hammer_on_creative.png">
<img src="/src/readme/hammer_how_mine.png">
<img src="/src/readme/putty_knife_craft.png">
<img src="/src/readme/putty_knife_large_craft.png">
<img src="/src/readme/putty_knife_size.png">
 


# Builds
- 1.0.0 Add 2 hammers
- 1.0.1 Change durability from 7000 to 5000
- 1.0.2 Add putty knife, large putty knife. Change texture from 16x16 to 32x32
- 1.0.3 fix: when place blocks by putty knife at first breaks old block and then place new.
- 1.0.4 Change the cost of resistance when breaking blocks from 1 and 2 to random(3,9) and random(5,25).
- 1.0.5 add minecraft 1.20.1 



- сборка проекта ./gradlew vscode
- билд ./gradlew build  (\hammer-me\build\libs)